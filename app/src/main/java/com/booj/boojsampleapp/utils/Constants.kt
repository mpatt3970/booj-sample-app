package com.booj.boojsampleapp.utils

import com.booj.boojsampleapp.BoojApp

class Constants {
    companion object {

        val TAG = BoojApp::class.simpleName
        const val BASE_URL = "https://www.denverrealestate.com"
        const val REALTOR_LIST_PATH = "rest.php/mobile/realtor/list?app_key=f7177163c833dff4b38fc8d2872f1ec6"
        const val CACHE_SIZE = (10 * 1024 * 1024).toLong()
        const val CACHE_DIR_NAME = "cache_dir"
        const val NETWORK_TIMEOUT = 10L
        const val REALTOR_INTENT_EXTRA = "extra_realtor"
    }
}