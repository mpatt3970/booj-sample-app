package com.booj.boojsampleapp.utils

import android.content.Context
import android.content.Intent
import com.booj.boojsampleapp.features.realtordetail.RealtorDetailActivity
import com.booj.boojsampleapp.models.domain.RealtorModel

// utils class with static navigation methods
class Navigator {

    companion object {

        fun toDetailActivity(context: Context, realtorModel: RealtorModel) {
            // TODO: it would be better to just pass an id and let the detail activity load the realtorModel for itself
            // but I didnt get around to adding a db
            val intent = Intent(context, RealtorDetailActivity::class.java)
            intent.putExtra(Constants.REALTOR_INTENT_EXTRA, realtorModel)
            // TODO: this is where we would start our transition animations
            context.startActivity(intent)
        }
    }
}
