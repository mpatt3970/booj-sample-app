package com.booj.boojsampleapp.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

class UtilMethods {

    companion object {

        // handles when a string is null by making it an empty string
        fun nullStringToEmptyString(str: String?): String {
            return str?.let { it } ?: ""
        }

        fun hasNetworkConnection(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            return activeNetwork?.isConnectedOrConnecting == true
        }
    }
}

fun String.concatWidthParameter(width: Int): String {
    return String.format("%s/width/%d", this, width)
}

fun String.concatLastName(lastName: String?): String {
    lastName?.let {
        return String.format("%s %s", this, it)
    }
    return this
}
