package com.booj.boojsampleapp.networking.api

import com.booj.boojsampleapp.models.response.RealtorResponse
import com.booj.boojsampleapp.utils.Constants
import kotlinx.coroutines.experimental.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface NetworkService {

    @GET(Constants.REALTOR_LIST_PATH)
    fun getRealtorList(): Deferred<Response<List<RealtorResponse>>>
}
