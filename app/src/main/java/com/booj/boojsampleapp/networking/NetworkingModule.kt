package com.booj.boojsampleapp.networking

import android.app.Application
import com.booj.boojsampleapp.networking.api.NetworkService
import com.booj.boojsampleapp.utils.Constants
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
class NetworkingModule {

    @Provides
    @Singleton
    @Named(Constants.CACHE_DIR_NAME)
    fun provideCacheDir(app: Application): File {
        return app.cacheDir
    }

    @Provides
    @Singleton
    fun provideNetworkCache(@Named(Constants.CACHE_DIR_NAME) cacheDir: File): Cache {
        return Cache(cacheDir, Constants.CACHE_SIZE)
    }

    @Provides
    @Singleton
    fun provideOkHttp(cache: Cache): OkHttpClient {
        // any more specific rules for the networking client like cache controls and the such would be defined here
        return OkHttpClient.Builder()
            .connectTimeout(Constants.NETWORK_TIMEOUT,  TimeUnit.SECONDS)
            .readTimeout(Constants.NETWORK_TIMEOUT,  TimeUnit.SECONDS)
            .writeTimeout(Constants.NETWORK_TIMEOUT,  TimeUnit.SECONDS)
            .cache(cache)
            .build()
    }

    // Moshi is similar to GSON but supports Kotlin and has stricter parsing rules
    @Provides
    @Singleton
    fun provideMoshi(): Moshi {
        return Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient, moshi: Moshi): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create()) // unbox primitives
            .addConverterFactory(MoshiConverterFactory.create(moshi)) // tell retrofit to use moshi for parsing
            .addCallAdapterFactory(CoroutineCallAdapterFactory()) // special adapter for Kotlin coroutine support
            .build()
    }

    @Provides
    fun provideNetworkService(retrofit: Retrofit): NetworkService {
        return retrofit.create(NetworkService::class.java)
    }
}
