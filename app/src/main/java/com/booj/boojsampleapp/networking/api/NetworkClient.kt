package com.booj.boojsampleapp.networking.api

import com.booj.boojsampleapp.models.response.RealtorResponse
import retrofit2.Response
import java.io.IOException
import javax.inject.Inject

class NetworkClient @Inject constructor(val networkService: NetworkService) {

    @Throws(IOException::class)
    // declared as a suspend function to inform the coroutine that it should wait for this call to finish
    suspend fun getRealtorListResponse(): List<RealtorResponse> {
        return try {
            // the await call here changes the "Deferred" kotlin type into a normal Retrofit response
            // await can only be called inside coroutines or other suspend functions
            unwrapResponse(networkService.getRealtorList().await())
        } catch (e: IOException) {
            throw e
        }
    }

    @Throws(IOException::class)
    fun <T> unwrapResponse(response: Response<T>): T {
        // generic network successful response checks, could be made more specific for more specific network error types
        if (response.isSuccessful) {
            response.body()?.let {
                return it
            } ?: run {
                throw IOException("Body was empty in the response")
            }
        } else {
            throw IOException("Call was not successful")
        }
    }
}
