package com.booj.boojsampleapp.networking

import com.booj.boojsampleapp.models.domain.RealtorModel
import com.booj.boojsampleapp.networking.api.NetworkClient
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Job
import kotlinx.coroutines.experimental.launch
import java.io.IOException
import javax.inject.Inject

class NetworkManager @Inject constructor(val networkClient: NetworkClient) {

    // coroutine rootParent job
    var rootJob: Job? = null

    // accepts higher order functions as parameters, cause functional programming
    fun getRealtorList(
        callbackSuccess: (List<RealtorModel>) -> Any,
        callbackError: (Throwable) -> Any
    ) {
        // better to null the job out and recreate it because once cancelled, it doesnt seem possible to restore a job instance
        if (rootJob == null) {
            rootJob = Job()
        }
        // starts a coroutine, CommonPool is default, this could be replaced if its limited size becomes an issue
        launch(CommonPool, parent = rootJob) {
            try {
                // this is the async task that we need to wait for completion
                val list = networkClient.getRealtorListResponse()
                // if there's more specific logic that belongs in the back end like populating a DB or special conversions, they would go here
                // this line wont fire until the async task above is finished, that's the coroutine way
                callbackSuccess(RealtorModel.convertList(list))
            } catch (e: IOException) {
                callbackError(e)
            }
        }
    }

    fun teardown() {
        // cancels every coroutine that was started with this job as the parent
        rootJob?.cancel()
        rootJob = null
    }

}
