package com.booj.boojsampleapp.models.domain

import android.os.Parcel
import android.os.Parcelable
import com.booj.boojsampleapp.models.response.RealtorResponse
import com.booj.boojsampleapp.utils.UtilMethods

data class RealtorModel(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val rebrand: String,
    val office: String,
    val isTeam: Boolean,
    val phoneNumber: String,
    val photo: String,
    val title: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(rebrand)
        parcel.writeString(office)
        parcel.writeByte(if (isTeam) 1 else 0)
        parcel.writeString(phoneNumber)
        parcel.writeString(photo)
        parcel.writeString(title)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RealtorModel> {
        override fun createFromParcel(parcel: Parcel): RealtorModel {
            return RealtorModel(parcel)
        }

        override fun newArray(size: Int): Array<RealtorModel?> {
            return arrayOfNulls(size)
        }

        // static converters from response models to domain models
        fun convertList(realtorListResponse: List<RealtorResponse>): List<RealtorModel> {
            return realtorListResponse.map {
                convertOne(it)
            }
        }

        fun convertOne(realtorResponse: RealtorResponse): RealtorModel {
            return RealtorModel(
                realtorResponse.id,
                realtorResponse.first_name,
                UtilMethods.nullStringToEmptyString(realtorResponse.last_name),
                realtorResponse.rebrand,
                realtorResponse.office,
                realtorResponse.is_team,
                realtorResponse.phone_number,
                realtorResponse.photo,
                UtilMethods.nullStringToEmptyString(realtorResponse.title)
            )
        }
    }
}
