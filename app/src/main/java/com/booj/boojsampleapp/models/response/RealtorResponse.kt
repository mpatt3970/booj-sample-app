package com.booj.boojsampleapp.models.response

// matches 1-1 with the network response's json, gets converted into a domain model
data class RealtorResponse(
    val id: Int,
    val first_name: String,
    val last_name: String?,
    val rebrand: String,
    val office: String,
    val is_team: Boolean,
    val phone_number: String,
    val photo: String,
    val title: String?
)