package com.booj.boojsampleapp

import com.booj.boojsampleapp.features.realtordetail.RealtorDetailActivity
import com.booj.boojsampleapp.features.realtorslist.RealtorAdapter
import com.booj.boojsampleapp.features.realtorslist.RealtorListActivity
import com.booj.boojsampleapp.networking.NetworkingModule
import dagger.Component
import javax.inject.Singleton

@Singleton
// this adds all the Provides methods of these modules to this component
@Component(modules = arrayOf(NetworkingModule::class, MainModule::class))
interface BoojComponent {

    // not necessary but in a more complex app it would be
    fun inject(boojApp: BoojApp)

    fun inject(realtorListActivity: RealtorListActivity)
    fun inject(realtorAdapter: RealtorAdapter)
    fun inject(realtorDetailActivity: RealtorDetailActivity)
}
