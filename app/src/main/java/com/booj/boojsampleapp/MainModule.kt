package com.booj.boojsampleapp

import android.app.Application
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MainModule constructor(private val app: Application) {

    // bare bones dagger provider for the application

    @Provides
    @Singleton
    fun provideApp(): Application {
        return app
    }

    @Provides
    @Singleton
    fun providePicasso(): Picasso {
        return Picasso.Builder(app)
            .indicatorsEnabled(false) // allows us to verify where the app image is coming from
            .build()
    }
}
