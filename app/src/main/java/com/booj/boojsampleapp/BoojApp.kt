package com.booj.boojsampleapp

import android.app.Application
import com.booj.boojsampleapp.networking.NetworkingModule

class BoojApp : Application() {

    companion object {
        // component created at init of application
        @JvmStatic lateinit var boojComponent: BoojComponent

        // provides the component to allow classes to call inject(this)
        fun getComponent(): BoojComponent {
            return boojComponent
        }
    }

    init {
        boojComponent = DaggerBoojComponent.builder()
            .networkingModule(NetworkingModule())
            .mainModule(MainModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        boojComponent.inject(this)
    }
}
