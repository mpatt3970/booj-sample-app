package com.booj.boojsampleapp.features.realtorslist

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.booj.boojsampleapp.BoojApp
import com.booj.boojsampleapp.R
import com.booj.boojsampleapp.models.domain.RealtorModel
import com.booj.boojsampleapp.networking.NetworkManager
import com.booj.boojsampleapp.utils.Constants
import com.booj.boojsampleapp.utils.UtilMethods
import kotlinx.android.synthetic.main.error_state.*
import kotlinx.android.synthetic.main.loading_state.*
import kotlinx.android.synthetic.main.no_network_state.*
import kotlinx.android.synthetic.main.realtor_list_success_state.*
import javax.inject.Inject

class RealtorListActivity : AppCompatActivity() {

    companion object {
        const val LOADING_STATE = 0
        const val NO_NETWORK_STATE = 1
        const val SUCCESS_STATE = 2
        const val ERROR_STATE = 3
    }

    @Inject
    // protected because Dagger won't inject into private fields
    protected lateinit var networkManager: NetworkManager

    private val layoutManager = LinearLayoutManager(this)
    private val realtorAdapter = RealtorAdapter(this)
    private val viewStateLists = mutableListOf<View>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_realtor_list)
        BoojApp.getComponent().inject(this)
        viewStateLists.addAll(listOf(loadingState, noNetworkState, errorState, successState))
        updateStateVisibility(LOADING_STATE)
        realtorList.layoutManager = layoutManager
        realtorList.adapter = realtorAdapter
        val dividerItemDecoration = DividerItemDecoration(this, layoutManager.orientation)
        realtorList.addItemDecoration(dividerItemDecoration)

        // TODO: If there was a header requirement, I would have a coordinatorLayout getting setup here
    }

    override fun onResume() {
        super.onResume()
        if (UtilMethods.hasNetworkConnection(this)) {
            networkManager.getRealtorList(callbackSuccess, callbackError)
        } else {
            updateStateVisibility(NO_NETWORK_STATE)
        }
    }

    override fun onPause() {
        super.onPause()
        // always tear down our async tasks when the user backgrounds the app
        networkManager.teardown()
    }

    // this value is a function defined as (List<Realtor>) -> Any
    val callbackSuccess = { realtors: List<RealtorModel> ->
        Log.d(Constants.TAG, realtors.toString())
        runOnUiThread {
            realtorAdapter.updateRealtorList(realtors)
            updateStateVisibility(SUCCESS_STATE)
        }
    }

    // (Throwable) -> Any
    val callbackError = { throwable: Throwable ->
        Log.d(Constants.TAG, throwable.message, throwable)
        runOnUiThread {
            updateStateVisibility(ERROR_STATE)
        }
    }


    fun updateStateVisibility(state: Int) {
        var visibleViewState: View? = null
        when (state) {
            LOADING_STATE -> visibleViewState = loadingState
            NO_NETWORK_STATE -> visibleViewState = noNetworkState
            SUCCESS_STATE -> visibleViewState = successState
            ERROR_STATE -> visibleViewState = errorState
        }
        viewStateLists.forEach {
            if (it != visibleViewState) {
                it.visibility = View.GONE
            }
        }
        visibleViewState?.visibility = View.VISIBLE
    }
}
