package com.booj.boojsampleapp.features.realtorslist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.booj.boojsampleapp.BoojApp
import com.booj.boojsampleapp.R
import com.booj.boojsampleapp.models.domain.RealtorModel
import com.booj.boojsampleapp.utils.Navigator
import com.booj.boojsampleapp.utils.concatLastName
import com.booj.boojsampleapp.utils.concatWidthParameter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.realtor_list_item.view.*
import javax.inject.Inject

class RealtorAdapter(val context: Context) : RecyclerView.Adapter<RealtorViewHolder>() {

    @Inject
    protected lateinit var picasso: Picasso

    val realtorList = mutableListOf<RealtorModel>()

    init {
        BoojApp.getComponent().inject(this)
    }

    fun updateRealtorList(list: List<RealtorModel>) {
        // TODO: this can be made more performant using DiffUtils
        realtorList.clear()
        realtorList.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return realtorList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RealtorViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.realtor_list_item, parent, false)
        val width = context.resources.getDimensionPixelOffset(R.dimen.listImageWidth)
        return RealtorViewHolder(view, picasso, width)
    }

    override fun onBindViewHolder(holder: RealtorViewHolder?, position: Int) {
        holder?.update(realtorList[position])
    }

}

class RealtorViewHolder(itemView: View, val picasso: Picasso, val width: Int) : RecyclerView.ViewHolder(itemView) {

    fun update(realtor: RealtorModel) {
        val sizedUrl = realtor.photo.concatWidthParameter(width)
        picasso.load(sizedUrl).placeholder(R.drawable.image_placeholder)
            .into(itemView.realtorImage)
        itemView.realtorName.text = realtor.firstName.concatLastName(realtor.lastName)
        itemView.realtorNumber.text = realtor.phoneNumber
        itemView.setOnClickListener {
            Navigator.toDetailActivity(it.context, realtor)
        }
    }
}