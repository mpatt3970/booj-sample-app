package com.booj.boojsampleapp.features.realtordetail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.booj.boojsampleapp.BoojApp
import com.booj.boojsampleapp.R
import com.booj.boojsampleapp.models.domain.RealtorModel
import com.booj.boojsampleapp.utils.Constants
import com.booj.boojsampleapp.utils.concatLastName
import com.booj.boojsampleapp.utils.concatWidthParameter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_realtor_detail.*
import javax.inject.Inject

class RealtorDetailActivity : AppCompatActivity() {

    @Inject
    lateinit var picasso: Picasso

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scrollable_activity_realtor_detail)
        BoojApp.getComponent().inject(this)

        // TODO: it would be nice if the model came loaded from a DB and we only passed an id thru
        // but for this app, passing the parcelable works fine

        // get the model from intent and populate the view
        val model = intent.extras.getParcelable<RealtorModel>(Constants.REALTOR_INTENT_EXTRA)
        val imageWidth = resources.getDimensionPixelOffset(R.dimen.detailImageWidth)
        val imageUrl = model.photo.concatWidthParameter(imageWidth)
        picasso.load(imageUrl).placeholder(R.drawable.image_placeholder).into(detailImage)
        realtorDetailName.text = model.firstName.concatLastName(model.lastName)
        realtorDetailOffice.text = model.office
        realtorDetailNumber.text = model.phoneNumber
        if (model.isTeam) {
            detailOnTeam.visibility = View.VISIBLE
        }
    }
}
